
locals {
  db_to   = 3306
  db_from = 3306

  ssh_to   = 22
  ssh_from = 22

  http_to   = 80
  http_from = 80

  msk_to   = 443
  msk_from = 443

}

resource "aws_security_group" "sg" {
  name        = "master_sg"
  description = "Access to the Baston host, EC2_instances, DB and web from the VPC"
  vpc_id      = aws_vpc.vpc.id

  ingress {
    description = "DB"
    from_port   = local.db_from
    to_port     = local.db_to
    protocol    = "tcp"
    cidr_blocks = [var.aws_cidr]
  }

  ingress {
    description = "DB"
    from_port   = local.msk_from
    to_port     = local.msk_to
    protocol    = "tcp"
    cidr_blocks = [var.aws_cidr]
  }

  ingress {
    description = "SSH Access"
    from_port   = local.ssh_from
    to_port     = local.ssh_to
    protocol    = "tcp"
    cidr_blocks = [var.aws_cidr]
  }

  ingress {
    description = "HTTP Access"
    from_port   = local.http_from
    to_port     = local.http_to
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "sg"
  }
}