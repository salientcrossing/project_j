variable "aws_cidr" {
  type    = string
  default = "10.1.0.0/16"
}

variable "env" {
  type    = string
  default = "proj31"
}

variable "main" {
  type    = string
  default = "vpc"
}

variable "az_count" {
  description = "Number of AZs to cover in a given AWS region"
  default     = "2"
}